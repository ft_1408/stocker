<?php declare(strict_types=1);

namespace App\Entity;

use App\Entity\Embeddable\Cost;
use App\Entity\Embeddable\ShippingDetails;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 * @ORM\Table(name="`order`")
 */
class Order
{
    public const SHIPPING_STANDARD = 'standard';
    public const SHIPPING_EXPRESS = 'express';

    public const AVAILABLE_SHIPPING_TYPES = [
        self::SHIPPING_STANDARD,
        self::SHIPPING_EXPRESS
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"order_details"})
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Product", inversedBy="orders")
     * @ORM\JoinTable(
     *      joinColumns={@ORM\JoinColumn(name="order_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="product_sku", referencedColumnName="sku")}
     *      )
     * @Groups({"order_details"})
     */
    private $products;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="orders")
     * @Groups({"order_details_create"})
     */
    private $user;

    /**
     * @ORM\Column(type="string")
     * @Groups({"order_details"})
     *
     * @var string
     */
    private $shippingType;

    /**
     * @ORM\Embedded(class="App\Entity\Embeddable\Cost")
     * @Groups({"order_details"})
     *
     * @var Cost
     */
    private $cost;

    /**
     * @ORM\Embedded(class="App\Entity\Embeddable\ShippingDetails")
     * @Groups({"order_details"})
     *
     * @var ShippingDetails
     */
    private $shippingDetails;

    public function __construct()
    {
        $this->cost = new Cost();
        $this->shippingDetails = new ShippingDetails();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|null
     */
    public function getProducts(): ?Collection
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     * @return Order
     */
    public function setProducts($products)
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Order
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getShippingType(): ?string
    {
        return $this->shippingType;
    }

    /**
     * @param string $shippingType
     * @return Order
     */
    public function setShippingType(string $shippingType): Order
    {
        $this->shippingType = $shippingType;
        return $this;
    }

    /**
     * @return Cost
     */
    public function getCost(): Cost
    {
        return $this->cost;
    }

    /**
     * @param Cost $cost
     * @return Order
     */
    public function setCost(Cost $cost): Order
    {
        $this->cost = $cost;
        return $this;
    }

    /**
     * @return ShippingDetails
     */
    public function getShippingDetails(): ShippingDetails
    {
        return $this->shippingDetails;
    }

    /**
     * @param ShippingDetails $shippingDetails
     * @return Order
     */
    public function setShippingDetails(ShippingDetails $shippingDetails): Order
    {
        $this->shippingDetails = $shippingDetails;
        return $this;
    }
}