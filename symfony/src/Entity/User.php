<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user_details"})
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Groups({"user_details"})
     *
     * @var string
     */
    private $nameFirst;


    /**
     * @ORM\Column(type="string")
     * @Groups({"user_details"})
     *
     * @var string
     */
    private $nameLast;


    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups({"user_details"})
     *
     * @var string
     */
    private $balance;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="user")
     *
     * @var Collection
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Order", mappedBy="user")
     *
     * @var Collection
     */
    private $orders;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNameFirst(): ?string
    {
        return $this->nameFirst;
    }

    /**
     * @param string $nameFirst
     * @return User
     */
    public function setNameFirst(string $nameFirst): User
    {
        $this->nameFirst = $nameFirst;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNameLast(): ?string
    {
        return $this->nameLast;
    }

    /**
     * @param string $nameLast
     * @return User
     */
    public function setNameLast(string $nameLast): User
    {
        $this->nameLast = $nameLast;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBalance(): ?string
    {
        return $this->balance;
    }

    /**
     * @param string $balance
     * @return User
     */
    public function setBalance(string $balance): User
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getProducts(): ?Collection
    {
        return $this->products;
    }

    /**
     * @param Collection $products
     * @return User
     */
    public function setProducts(Collection $products): User
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getOrders(): ?Collection
    {
        return $this->orders;
    }
}