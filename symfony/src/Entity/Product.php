<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @UniqueEntity("sku")
 */
class Product
{
    public const PRODUCT_TYPE_MUG = 'mug';
    public const PRODUCT_TYPE_SHIRT = 't-shirt';

    public const AVAILABLE_SHIPPING_TYPES = [
        self::PRODUCT_TYPE_MUG,
        self::PRODUCT_TYPE_SHIRT
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @Groups({"product_details", "product_details_create"})
     *
     * @var string
     */
    private $sku;

    /**
     * @ORM\Column(type="string")
     * @Groups({"product_details", "product_details_create"})
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="string")
     * @Groups({"product_details", "product_details_create"})
     *
     * @var string
     */
    private $type;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups({"product_details", "product_details_create"})
     *
     * @var string
     */
    private $cost;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="products")
     * @Groups({"product_details_create"})
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Order", inversedBy="products")
     * @ORM\JoinTable(
     *      joinColumns={@ORM\JoinColumn(name="product_sku", referencedColumnName="sku")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="order_id", referencedColumnName="id")}
     *      )
     */
    private $orders;

    /**
     * @return string
     */
    public function getSku(): ?string
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     * @return Product
     */
    public function setSku(string $sku): Product
    {
        $this->sku = $sku;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Product
     */
    public function setTitle(string $title): Product
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getCost(): ?string
    {
        return $this->cost;
    }

    /**
     * @param string $cost
     * @return Product
     */
    public function setCost(string $cost): Product
    {
        $this->cost = $cost;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Product
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Product
     */
    public function setType(string $type): Product
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param mixed $orders
     * @return Product
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;
        return $this;
    }
}