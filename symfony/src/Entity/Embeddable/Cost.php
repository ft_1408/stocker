<?php declare(strict_types=1);

namespace App\Entity\Embeddable;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/** @ORM\Embeddable() */
class Cost
{
    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups({"order_details"})
     *
     * @var string
     */
    private $products;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups({"order_details"})
     *
     * @var string
     */
    private $shipping;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups({"order_details"})
     *
     * @var string
     */
    private $total;

    /**
     * @return string
     */
    public function getProducts(): ?string
    {
        return $this->products;
    }

    /**
     * @param string $products
     * @return Cost
     */
    public function setProducts(string $products): Cost
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @return string
     */
    public function getShipping(): ?string
    {
        return $this->shipping;
    }

    /**
     * @param string $shipping
     * @return Cost
     */
    public function setShipping(string $shipping): Cost
    {
        $this->shipping = $shipping;
        return $this;
    }

    /**
     * @return string
     */
    public function getTotal(): ?string
    {
        return $this->total;
    }

    /**
     * @param string $total
     * @return Cost
     */
    public function setTotal(string $total): Cost
    {
        $this->total = $total;
        return $this;
    }
}