<?php declare(strict_types=1);

namespace App\Entity\Embeddable;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/** @ORM\Embeddable() */
class ShippingDetails
{
    public const SHIPPING_TYPE_DOMESTIC = 'domestic';
    public const SHIPPING_TYPE_INTERNATIONAL = 'international';

    public const AVAILABLE_SHIPPING_TYPES = [
        self::SHIPPING_TYPE_DOMESTIC,
        self::SHIPPING_TYPE_INTERNATIONAL
    ];

    /**
     * @ORM\Column(type="string")
     * @Groups({"order_details"})
     *
     * @var string
     */
    private $type;

    /**
     * @ORM\Column(type="string")
     * @Groups({"order_details"})
     *
     * @var string
     */
    private $fullName;

    /**
     * @ORM\Column(type="string")
     * @Groups({"order_details"})
     *
     * @var string
     */
    private $address;

    /**
     * @ORM\Column(type="string")
     * @Groups({"order_details"})
     *
     * @var string
     */
    private $country;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"order_details"})
     *
     * @var string
     */
    private $state;

    /**
     * @ORM\Column(type="string")
     * @Groups({"order_details"})
     *
     * @var string
     */
    private $city;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"order_details"})
     *
     * @var string
     */
    private $zip;

    /**
     * @ORM\Column(type="string")
     * @Groups({"order_details"})
     *
     * @var string
     */
    private $phone;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"order_details"})
     *
     * @var string
     */
    private $region;

    /**
     * @return string
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     * @return ShippingDetails
     */
    public function setFullName(string $fullName): ShippingDetails
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return ShippingDetails
     */
    public function setAddress(string $address): ShippingDetails
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return ShippingDetails
     */
    public function setCountry(string $country): ShippingDetails
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * @param string $state
     * @return ShippingDetails
     */
    public function setState(string $state): ShippingDetails
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return ShippingDetails
     */
    public function setCity(string $city): ShippingDetails
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getZip(): ?string
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     * @return ShippingDetails
     */
    public function setZip(string $zip): ShippingDetails
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return ShippingDetails
     */
    public function setPhone(string $phone): ShippingDetails
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @param string $region
     * @return ShippingDetails
     */
    public function setRegion(string $region): ShippingDetails
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return ShippingDetails
     */
    public function setType(string $type): ShippingDetails
    {
        $this->type = $type;
        return $this;
    }
}