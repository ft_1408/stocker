<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200321142341 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product_order (product_sku VARCHAR(255) NOT NULL, order_id INT NOT NULL, INDEX IDX_5475E8C4EFBF6CDB (product_sku), INDEX IDX_5475E8C48D9F6D38 (order_id), PRIMARY KEY(product_sku, order_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `order` (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, shipping_type VARCHAR(255) NOT NULL, cost_products NUMERIC(10, 2) NOT NULL, cost_shipping NUMERIC(10, 2) NOT NULL, cost_total NUMERIC(10, 2) NOT NULL, shipping_details_full_name VARCHAR(255) NOT NULL, shipping_details_address VARCHAR(255) NOT NULL, shipping_details_country VARCHAR(255) NOT NULL, shipping_details_state VARCHAR(255) NOT NULL, shipping_details_city VARCHAR(255) NOT NULL, shipping_details_zip VARCHAR(255) DEFAULT NULL, shipping_details_phone VARCHAR(255) NOT NULL, shipping_details_region VARCHAR(255) DEFAULT NULL, INDEX IDX_F5299398A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_product (order_id INT NOT NULL, product_sku VARCHAR(255) NOT NULL, INDEX IDX_2530ADE68D9F6D38 (order_id), INDEX IDX_2530ADE6EFBF6CDB (product_sku), PRIMARY KEY(order_id, product_sku)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_order ADD CONSTRAINT FK_5475E8C4EFBF6CDB FOREIGN KEY (product_sku) REFERENCES product (sku)');
        $this->addSql('ALTER TABLE product_order ADD CONSTRAINT FK_5475E8C48D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F5299398A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE order_product ADD CONSTRAINT FK_2530ADE68D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE order_product ADD CONSTRAINT FK_2530ADE6EFBF6CDB FOREIGN KEY (product_sku) REFERENCES product (sku)');
        $this->addSql('ALTER TABLE product ADD type VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_order DROP FOREIGN KEY FK_5475E8C48D9F6D38');
        $this->addSql('ALTER TABLE order_product DROP FOREIGN KEY FK_2530ADE68D9F6D38');
        $this->addSql('DROP TABLE product_order');
        $this->addSql('DROP TABLE `order`');
        $this->addSql('DROP TABLE order_product');
        $this->addSql('ALTER TABLE product DROP type');
    }
}
