<?php declare(strict_types=1);

namespace App\Controller;

use App\Form\Type\User\CreateType;
use App\Repository\UserRepository;
use App\Service\ApiResponse;
use App\Service\UserManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController
{
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var ApiResponse
     */
    private $apiResponse;

    public function __construct(UserManager $userManager, FormFactoryInterface $formFactory, ApiResponse $apiResponse)
    {
        $this->userManager = $userManager;
        $this->formFactory = $formFactory;
        $this->apiResponse = $apiResponse;
    }

    /**
    * @Route("/user", name="app_user_create", methods={"POST"})
    */
    public function createUser(Request $request): Response
    {
        $form = $this->formFactory->createNamed('', CreateType::class);
        $form->handleRequest($request);

        if (($form->isSubmitted() && $form->isValid())) {
            $savedUser = $this->userManager->createUser($form->getData());

            return $this->apiResponse->createOkResponse($savedUser, ['user_details']);
        }

        if ($form->getErrors()) {
            return $this->apiResponse->createFormErrorsResponse($form);
        }

        return $this->apiResponse->createErrorsResponse(['Please submit a form']);
    }
}