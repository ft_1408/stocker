<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Form\Type\Product\CreateType;
use App\Service\ApiResponse;
use App\Service\ProductManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var ApiResponse
     */
    private $apiResponse;
    /**
     * @var ProductManager
     */
    private $productManager;


    public function __construct(FormFactoryInterface $formFactory, ApiResponse $apiResponse, ProductManager $productManager)
    {
        $this->formFactory = $formFactory;
        $this->apiResponse = $apiResponse;
        $this->productManager = $productManager;
    }

    /**
     * @Route("/product", name="app_product_create", methods={"POST"})
     */
    public function createProduct(Request $request): Response
    {
        $form = $this->formFactory->createNamed('', CreateType::class);
        $form->handleRequest($request);

        if (($form->isSubmitted() && $form->isValid())) {
            $product = $this->productManager->createProduct($form->getData());
            return $this->apiResponse->createOkResponse($product, ['product_details_create', 'user_details']);
        }

        if ($form->getErrors()) {
            return $this->apiResponse->createFormErrorsResponse($form);
        }

        return $this->apiResponse->createErrorsResponse(['Please submit a form']);
    }

    /**
     * @Route("/product/{user}", name="app_product_get_by_user", methods={"GET"})
     *
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getByUser(User $user): Response
    {
        return $this->apiResponse->createOkResponse($user->getProducts()->toArray(), ['product_details']);
    }
}