<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Form\Type\Order\CreateType;
use App\Service\ApiResponse;
use App\Service\OrderManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController
{
    /**
     * @var OrderManager
     */
    private $orderManager;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var ApiResponse
     */
    private $apiResponse;

    public function __construct(FormFactoryInterface $formFactory, ApiResponse $apiResponse, OrderManager $orderManager)
    {
        $this->formFactory = $formFactory;
        $this->apiResponse = $apiResponse;
        $this->orderManager = $orderManager;
    }

    /**
     * @Route("/order", name="app_order_create", methods={"POST"})
     */
    public function createProduct(Request $request): Response
    {
        $form = $this->formFactory->createNamed('', CreateType::class);
        $form->handleRequest($request);


        if (($form->isSubmitted() && $form->isValid())) {
            $order = $this->orderManager->finishOrder($form->getData());
            return $this->apiResponse->createOkResponse($order, ['order_details', 'product_details', 'user_details', 'order_details_create']);
        }

        if ($form->getErrors()) {
            return $this->apiResponse->createFormErrorsResponse($form);
        }

        return $this->apiResponse->createErrorsResponse(['Please submit a form']);
    }

    /**
     * @Route("/order/{user}", name="app_order_get_by_user", methods={"GET"})
     *
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getByUser(User $user): Response
    {
        return $this->apiResponse->createOkResponse($user->getOrders()->toArray(), ['order_details', 'product_details']);
    }
}