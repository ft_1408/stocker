<?php declare(strict_types=1);

namespace App\Form\Type\Product;

use App\Entity\Product;
use App\Form\DataTransformer\UserToIdTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class CreateType extends AbstractType
{

    /**
     * @var UserToIdTransformer
     */
    private $userTransformer;

    public function __construct(UserToIdTransformer $userTransformer)
    {
        $this->userTransformer = $userTransformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param mixed[]              $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('sku', TextType::class, ['constraints' => [new NotBlank()]])
            ->add('title', TextType::class, ['constraints' => [new NotBlank()]])
            ->add('type', ChoiceType::class, ['constraints' => [new NotBlank()], 'choices' => Product::AVAILABLE_SHIPPING_TYPES])
            ->add('cost', TextType::class, ['constraints' => [new NotBlank()]])
            ->add('user', TextType::class, ['constraints' => [new NotBlank()]])
        ;

        $builder->get('user')
            ->addModelTransformer($this->userTransformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
            'csrf_protection' => false,
        ]);
    }
}