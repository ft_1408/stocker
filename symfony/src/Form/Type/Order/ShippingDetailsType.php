<?php declare(strict_types=1);

namespace App\Form\Type\Order;

use App\Entity\Embeddable\ShippingDetails;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ShippingDetailsType extends AbstractType
{
    private const GROUP_SHIPPING_DETAILS_DOMESTIC = 'shipping_details_domestic';
    private const GROUP_SHIPPING_DETAILS_INTERNATIONAL = 'shipping_details_international';

    /**
     * @param FormBuilderInterface $builder
     * @param mixed[]              $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', ChoiceType::class,
                [
                    'constraints' => [new NotBlank()],
                    'choices' => ShippingDetails::AVAILABLE_SHIPPING_TYPES,
                ]
            )
            ->add('fullName', TextType::class, ['constraints' => [new NotBlank(['groups'=>self::GROUP_SHIPPING_DETAILS_INTERNATIONAL])]])
            ->add('address', TextType::class, ['constraints' => [new NotBlank(['groups'=>self::GROUP_SHIPPING_DETAILS_INTERNATIONAL])]])
            ->add('country', TextType::class, ['constraints' => [new NotBlank(['groups'=>self::GROUP_SHIPPING_DETAILS_INTERNATIONAL])]])
            ->add('city', TextType::class, ['constraints' => [new NotBlank(['groups'=>self::GROUP_SHIPPING_DETAILS_INTERNATIONAL])]])
            ->add('phone', TextType::class, ['constraints' => [new NotBlank(['groups'=>self::GROUP_SHIPPING_DETAILS_INTERNATIONAL])]])

            ->add('zip', TextType::class, ['constraints' => [new NotBlank(['groups'=>self::GROUP_SHIPPING_DETAILS_DOMESTIC])]])
            ->add('state', TextType::class, ['constraints' => [new NotBlank(['groups'=>self::GROUP_SHIPPING_DETAILS_DOMESTIC])]])

            ->add('region', TextType::class)
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ShippingDetails::class,
            'csrf_protection' => false,
            'validation_groups' => function (FormInterface $form) {
                $data = $form->getData();

                $groups = [self::GROUP_SHIPPING_DETAILS_INTERNATIONAL];
                if (ShippingDetails::SHIPPING_TYPE_DOMESTIC == $data->getType()) {
                    $groups[] = self::GROUP_SHIPPING_DETAILS_DOMESTIC;
                }

                return $groups;
            },
        ]);
    }
}