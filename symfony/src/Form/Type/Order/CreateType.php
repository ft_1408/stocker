<?php declare(strict_types=1);

namespace App\Form\Type\Order;

use App\Entity\Embeddable\ShippingDetails;
use App\Entity\Order;
use App\Form\DataTransformer\ProductToIdTransformer;
use App\Form\DataTransformer\UserToIdTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class CreateType extends AbstractType
{

    /**
     * @var ProductToIdTransformer
     */
    private $transformer;
    /**
     * @var UserToIdTransformer
     */
    private $userToIdTransformer;

    public function __construct(ProductToIdTransformer $transformer, UserToIdTransformer $userToIdTransformer)
    {
        $this->transformer = $transformer;
        $this->userToIdTransformer = $userToIdTransformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param mixed[]              $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('products', CollectionType::class,
                    [
                        'constraints' => [new NotBlank()],
                        'allow_add' => true,
                    ]
            )
            ->add('shippingDetails', ShippingDetailsType::class,
                [
                    'constraints' => [new NotBlank()],
                    'data_class' => ShippingDetails::class,
                ]
            )
            ->add('user', TextType::class, ['constraints' => [new NotBlank()]])
            ->add('shippingType', ChoiceType::class, ['constraints' => [new NotBlank()], 'choices' => Order::AVAILABLE_SHIPPING_TYPES])
        ;

        $builder->get('products')
            ->addModelTransformer($this->transformer);

        $builder->get('user')
            ->addModelTransformer($this->userToIdTransformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
            'csrf_protection' => false,
        ]);
    }
}