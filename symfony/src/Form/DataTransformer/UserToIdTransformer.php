<?php declare(strict_types=1);

namespace App\Form\DataTransformer;

use App\Repository\UserRepository;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class UserToIdTransformer implements DataTransformerInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function transform($user)
    {
        if (null === $user) {
            return '';
        }

        return $user->getId();
    }

    public function reverseTransform($userId)
    {
        if (!$userId) {
            return;
        }

        $user = $this->userRepository
            ->find($userId)
        ;

        if (null === $user) {
            throw new TransformationFailedException(sprintf(
                'User with ID "%s" does not exist!',
                $userId
            ));
        }

        return $user;
    }
}