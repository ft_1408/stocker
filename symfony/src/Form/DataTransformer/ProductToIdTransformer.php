<?php declare(strict_types=1);

namespace App\Form\DataTransformer;

use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ProductToIdTransformer implements DataTransformerInterface
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }
    /**
     * @inheritDoc
     */
    public function transform($products)
    {
        if (null === $products) {
            return;
        }

        $productsIds = [];

        foreach ($products as $product) {
            $productsIds[] = $product->getSku();
        }

        return $productsIds;
    }

    /**
     * @inheritDoc
     */
    public function reverseTransform($productIds)
    {
        if (!$productIds) {
            return;
        }

        $products = new ArrayCollection();
        foreach ($productIds as $productId) {
            $product = $this->productRepository
                ->findOneBySku($productId)
            ;

            if (null === $product) {
                throw new TransformationFailedException(sprintf(
                    'Product with ID "%s" does not exist!',
                    $productId
                ));
            }

            $products->add($product);
        }

        return $products;
    }
}