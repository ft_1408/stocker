<?php declare(strict_types=1);

namespace App\Service;

use App\Entity\Embeddable\Cost;
use App\Entity\Order;
use App\Entity\Product;

class PriceProcessor
{
    /**
     * @var array
     */
    private $priceConfig;

    public function __construct(array $priceConfig)
    {
        // For better flexibility put it into DB
        $this->priceConfig = $priceConfig;
    }

    public function processFinalOrderCost(Order $order): Cost
    {
        $shippingTypeConfig = $this->priceConfig[$order->getShippingType()] ?? null;
        if ($shippingTypeConfig === null) {
            throw new \RuntimeException(sprintf('Shipping config is not set for %s shipping type', $order->getShippingType()));
        }

        $pricingDetails = $shippingTypeConfig[$order->getShippingDetails()->getType()] ?? null;

        if ($pricingDetails === null) {
            throw new \RuntimeException(sprintf('Shipping of this order is not available for %s address type', $order->getShippingDetails()->getType()));
        }

        return $this->formCostForOrderWithConfig($pricingDetails, $order);
    }

    private function formCostForOrderWithConfig(array $pricingConfig, Order $order): Cost
    {
        $cost = (new Cost())
            ->setProducts('0')
            ->setShipping('0')
            ->setTotal('0')
        ;

        foreach (Product::AVAILABLE_SHIPPING_TYPES as $productType) {
            $products = $order
                ->getProducts()
                ->filter(
                    static function (Product $product) use ($productType) {
                        return $product->getType() === $productType;
                    }
                );

            $qtyShippingConfig = $pricingConfig[$productType] ?? null;
            if ($qtyShippingConfig === null) {
                throw new \RuntimeException(sprintf('Qty config not set for product type %s', $productType));
            }

            $biggestQtyConfig = max(array_keys($qtyShippingConfig));
            $productCount = 0;

            /** @var Product $product */
            foreach ($products as $product) {
                $productCount++;

                $cost->setProducts(bcadd($cost->getProducts(), $product->getCost(), 2));
                $shippingCost = ($productCount >= $biggestQtyConfig) ? $qtyShippingConfig[$biggestQtyConfig] : $qtyShippingConfig[$productCount];

                $cost->setShipping(bcadd($cost->getShipping(), (string) $shippingCost, 2));
            }

        }

        $cost->setTotal(bcadd($cost->getShipping(), $cost->getProducts(), 2));

        return $cost;
    }
}