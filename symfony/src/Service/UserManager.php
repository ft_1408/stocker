<?php declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class UserManager
{
    /**
     * @var EntityManager
     */
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function createUser(User $newUser): User
    {
        $newUser->setBalance('100.00');

        $this->manager->persist($newUser);
        $this->manager->flush($newUser);

        return $newUser;
    }

    public function decreaseUserBalance(User $user, string $sum): User
    {
        if ($user->getBalance() < $sum) {
            throw new \RuntimeException('User has not enough money');
        }

        $user->setBalance(bcsub($user->getBalance(), $sum, 2));
        $this->manager->flush($user);

        return $user;
    }
}