<?php declare(strict_types=1);

namespace App\Service;

use App\Entity\Order;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class OrderManager
{
    /**
     * @var EntityManager
     */
    private $manager;
    /**
     * @var PriceProcessor
     */
    private $priceProcessor;
    /**
     * @var UserManager
     */
    private $userManager;

    public function __construct(EntityManagerInterface $manager, PriceProcessor $priceProcessor, UserManager $userManager)
    {
        $this->manager = $manager;
        $this->priceProcessor = $priceProcessor;
        $this->userManager = $userManager;
    }

    public function finishOrder(Order $order): Order
    {
        $cost = $this->priceProcessor->processFinalOrderCost($order);
        $order->setCost($cost);

        $this->manager->beginTransaction();

        try {
            $this->userManager->decreaseUserBalance($order->getUser(), $cost->getTotal());

            $this->manager->persist($order);
            $this->manager->flush($order);

            $this->manager->commit();
        } catch (\Exception $e) {
            $this->manager->rollback();

            throw $e;
        }

        return $order;
    }
}