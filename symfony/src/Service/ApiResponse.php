<?php declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Doctrine\Common\Annotations\AnnotationReader;


class ApiResponse
{
    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct()
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer($classMetadataFactory, $metadataAwareNameConverter)];

        $this->serializer = new Serializer($normalizers, $encoders);
    }

    public function createOkResponse($data, array $groups = []): JsonResponse
    {
        $jsonContent = $this->serializer->normalize($data, null, [
            'groups' => $groups,
        ]);

        return new JsonResponse($jsonContent);
    }

    public function createFormErrorsResponse(FormInterface $form): JsonResponse
    {
        $errors = [];

        foreach ($form->getErrors(true) as $error) {
            $errors[] = sprintf('%s: %s', $error->getCause()->getPropertyPath(), $error->getMessage());
        }

        return $this->createErrorsResponse($errors);
    }

    public function createErrorsResponse(array $errors, int $code = Response::HTTP_BAD_REQUEST) {
        return new JsonResponse($errors, $code);
    }
}